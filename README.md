# DoseBot for reddit

**DoseBot for reddit** is a port of [DoseBot](https://github.com/GabrielMorris/DoseBot) to reddit. **DoseBot** is a harm reduction Discord bot that can be used to provide server users with a variety of useful harm reduction information from the [PsychonautWiki](https://www.psychonautwiki.org) to promote safer use of recreational compounds.

**Harm reduction** is "the set of practical strategies and ideas at reducing negative consequences associated with drug use. Harm reduction is also a movement for social justice built on a belief in, and respect for, the rights of people who use drugs." [Harm Reduction Coalition](http://harmreduction.org/about-us/principles-of-harm-reduction/).

The **drug abuse epidemic** that has been sweeping the the United States has cost 63,600 lives in 2016 alone [CDC](https://www.cdc.gov/nchs/products/databriefs/db294.htm), with a median age of 20. Our hope is that by providing accurate, scientifically backed best practices for substance use that we may reduce this number significantly, but we consider even one life saved a success.

## Commands

- --combochart => Links a substance combination safety chart
- --dxmcalc [weight in lbs] => Calculates safe dosages for DXM by weight
- --effects [substance] => Provides a list of random effects for a given substance
- --info [substance] => Provides an information sheet on a given substance with various pieces of harm reduction information
- --psychtolerance [days since last trip] => Provides adjusted dosages accounting for tolerance, also provides a warning

## Contributors

Currently this project is maintained by Cocoa, voreskin, maethor, and josikinz.

## Usage

1.  Clone repo
2.  `npm install` to download dependencies
3.  `node bot.js` to start bot
