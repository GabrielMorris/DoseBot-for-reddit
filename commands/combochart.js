// Replies to a comment with the Tripsit drug combo safety chart
exports.run = (comment, args) => {
  console.log(
    `**********Executing combochart on ${
      comment.subreddit_name_prefixed
    }**********`
  );
  console.log(comment);
  // Send reply
  comment.reply(
    `[Drug combination chart](https://wiki.tripsit.me/images/3/3a/Combo_2.png)`
  );
};
