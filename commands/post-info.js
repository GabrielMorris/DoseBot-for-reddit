const compounds = require("../include/compounds.json");
const sanitizeSubstanceName = require("../include/sanitize-substance-name.js");
const { request } = require("graphql-request");

exports.run = post => {
  let title = post.title.toLowerCase().replace(/\?/g, "", -1);
  let titleArr = title.split(" ");
  console.log(titleArr);

  let found;
  titleArr.forEach(element => {
    for (let i = 0; i < compounds.data[0].length; i++) {
      if (compounds.data[0][i] == sanitizeSubstanceName(element)) {
        found = sanitizeSubstanceName(element);
      }
    }
  });
  console.log(`Found substance: ${found}`);

  if (!!found) {
    let drug = found.replace(/-/g, "", -1);
    // Sanitizes substance name to match PW API names
    drug = sanitizeSubstanceName(drug).trim();

    // Check if there is a custom sheet
    // Hardcoded to false for now
    hasCustom = false;

    if (hasCustom == false) {
      console.log(`Requesting info for ${drug}`);
      let query = require("../queries/info.js").info(drug);
      request("https://api.psychonautwiki.org", query)
        .then(data => {
          // Logs API's returned object of requested substance
          // console.log(data);

          // Set substance to the first returned substance from PW API
          let substance = data.substances[0];

          createPostReply(substance, post);
        })
        .catch(function(error) {
          console.log("Promise rejected/errored out");
          console.log(error);
        });

      // Reset hasCustom
      hasCustom = false;
    }
  } else {
    console.log("No drug found, ignoring post");
  }
};

function createPostReply(substance, post) {
  post.reply(
    `${buildTitleField(substance)}${buildChemicalClassField(
      substance
    )}\n\n${buildPsychoactiveClassField(substance)}\n\n${buildDosageField(
      substance
    )}\n${buildDurationField(substance)}${buildToleranceField(
      substance
    )}\n\n${buildAddictionPotentialField(substance)}\n\n${buildLinksField(
      substance
    )}${buildBotLinksField()}`
  );
}

function buildTitleField(substance) {
  return `### ${capitalize(substance.name)} Drug Information\n\n---\n\n`;
}

function buildDosageField(substance) {
  let messages = [`---\n\n#### **Dosages**\n\n`];

  for (let i = 0; i < substance.roas.length; i++) {
    let roa = substance.roas[i];
    let dose = roa.dose;
    let name = capitalize(roa.name);

    let dosageObjectToString = function(x) {
      // Set substance dose units
      let unit = dose.units;

      // If there's a dose return dose + unit
      if (!!x) {
        if (typeof x == "number") {
          return `${x}${unit}`;
        }
        // If there's a dose range return dose range + unit
        return `${x.min} - ${x.max}${unit}`;
      }
    };
    // custom sheet stuff
    // // If nonstandard dose add dosage info to messages array
    // if (!!dose) {
    //   messages.push(`${dose.dosage}`);
    //   messages.push("");
    // } else {
    //   // This should never really happen
    //   messages.push("No dosage information");
    // }
    if (!!dose) {
      // Add all the dosage info
      messages.push(`|**ROA**|**${name}**|`);
      messages.push(`|:-:|:-:|`);
      messages.push(
        `|Threshold|${dosageObjectToString(dose.threshold) ||
          "No information"}|`
      );
      messages.push(
        `|Light|${dosageObjectToString(dose.light) || "No information"}|`
      );
      messages.push(
        `|Common|${dosageObjectToString(dose.common) || "No information"}|`
      );
      messages.push(
        `|Strong|${dosageObjectToString(dose.strong) || "No information"}|`
      );
      messages.push(
        `|Heavy|${dosageObjectToString(dose.heavy) || "No information"}|`
      );
      messages.push("\n\n");
    } else {
      // Or none if there is none
      messages.push("No dosage information.");
    }
  }
  // Join the message array into a string
  return messages.join("\n");
}

// Functions
function buildDurationField(substance) {
  let messages = [`---\n\n#### **Duration**\n\n`];

  for (let i = 0; i < substance.roas.length; i++) {
    let roa = substance.roas[i];
    let dose = roa.dose;
    let name = capitalize(roa.name);

    let durationObjectToString = function(x) {
      // If there's a duration range return it + units
      if (!!x) {
        return `${x.min} - ${x.max} ${x.units}`;
      }
      return undefined;
    };

    if (!!roa.duration) {
      // Add all the dosage info
      messages.push(`|**ROA**|**${name}**|`);
      messages.push(`|:-:|:-:|`);
      messages.push(
        `|Onset|${durationObjectToString(roa.duration.onset) ||
          "No information"}|`
      );
      messages.push(
        `|Comeup|${durationObjectToString(roa.duration.comeup) ||
          "No information"}|`
      );
      messages.push(
        `|Peak|${durationObjectToString(roa.duration.peak) ||
          "No information"}|`
      );
      messages.push(
        `|Offset|${durationObjectToString(roa.duration.offset) ||
          "No information"}|`
      );
      messages.push(
        `|After effects|${durationObjectToString(roa.duration.afterglow) ||
          "No information"}|`
      );
      messages.push(
        `|**Total**|**${durationObjectToString(roa.duration.total) ||
          "No information"}**|`
      );
      messages.push("\n\n");
    } else {
      // Or none if there is none
      messages.push("No dosage information.");
    }
  }
  // Join the message array into a string
  return messages.join("\n");
}

function buildChemicalClassField(substance) {
  if (substance.class !== null) {
    if (substance.class.chemical !== null) {
      return `**Chemical class**: [${
        substance.class.chemical[0]
      }](https://psychonautwiki.org/wiki/${substance.class.chemical[0]})`;
    }
  } else {
    return "**Chemical class**: No information";
  }
}

function buildPsychoactiveClassField(substance) {
  if (substance.class !== null) {
    if (substance.class.psychoactive !== null) {
      return `**Psychoactive class**: [${
        substance.class.psychoactive[0]
      }](https://psychonautwiki.org/wiki/${substance.class.psychoactive[0]})`;
    }
  } else {
    return "**Psychoactive class**: No information";
  }
}

function buildAddictionPotentialField(substance) {
  if (substance.addictionPotential !== null) {
    console.log(substance);
    return `---\n\n#### **Addiction potential**\n\n${capitalize(
      substance.addictionPotential
    )}\n`;
  } else {
    return "---\n\n#### **Addiction potential**\n\nNo information";
  }
}

function buildToleranceField(substance) {
  if (substance.tolerance !== null) {
    let tolerances = substance.tolerance;

    if (!!tolerances) {
      // return standard tolerances
      return `---\n\n#### **Tolerance**\n\n**Full**: ${
        tolerances.full
      }\n\n**Half**: ${tolerances.half}\n\n**Baseline**: ${tolerances.zero}`;
    }
  } else {
    return `---\n\n#### **Tolerance**\n\nNo information`;
  }
}

function buildLinksField(substance) {
  return `\n---\n\n**More information:** [PsychonautWiki](https://psychonautwiki.org/wiki/${
    substance.name
  }) • [Tripsit](http://drugs.tripsit.me/${
    substance.name
  }) • [Combination chart](https://wiki.tripsit.me/images/3/3a/Combo_2.png)`;
}

function buildBotLinksField() {
  return `\n\n---\n\n[Effect Index](https://www.effectindex.com) • [Commands](https://www.reddit.com/r/DoseBot/comments/8xtj1h/commands/) • [Subreddit](https://www.reddit.com/r/DoseBot/) • [Github](https://github.com/GabrielMorris/dosebot-reddit)`;
}

function capitalize(name) {
  if (name === "lsa") {
    return name.toUpperCase();
  } else {
    return name[0].toUpperCase() + name.slice(1);
  }
}
