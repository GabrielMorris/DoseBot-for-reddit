// Usage --tolerance [days since last trip]. calculates tolerance/extra dose needed to achieve normal effects
exports.run = (comment, args) => {
  console.log(
    `**********Executing psychtolerance on ${
      comment.subreddit_name_prefixed
    }**********`
  );

  let str = comment.body;
  let result = str.split(" ");
  let x = parseFloat(result[result.length - 1]);
  let y = Math.pow(x - 14, 4) / 150 + (20 - (x / 14) * 20) + 100;

  comment.reply(generateToleranceMessage(x, y));
};

function generateToleranceMessage(x, y) {
  // If less than 14 days perform calcs, if greater return no tolerance
  if (x < 14) {
    if (x < 3) {
      // If days since last dose would give a very high dose return a warning
      // Uses \n\n or else it will end up as a code block due to indentation
      console.log(`${x} days`);
      return `### Psychedelic Tolerance Calculator\n\n---\n\nTake approximately ${generatePercentage(
        y
      )}% of the drug to reach full effects\n\n**Warning:** Negative effects may be amplified with a high dose of a psychedelic.\n\n---\n\n^(**[Effect Index](https://www.effectindex.com) • Commands • [Subreddit](https://www.reddit.com/r/DoseBot/) • [Discord](https://discord.gg/KvspajH) • [Github](https://github.com/GabrielMorris/dosebot-reddit)**)`;
    } else {
      console.log(`${x} days`);
      return `### Psychedelic Tolerance Calculator\n\n---\n\nTake approximately ${generatePercentage(
        y
      )}% of the drug to reach full effects\n\n---\n\n^(**[Effect Index](https://www.effectindex.com) • Commands • [Subreddit](https://www.reddit.com/r/DoseBot/) • [Discord](https://discord.gg/KvspajH) • [Github](https://github.com/GabrielMorris/dosebot-reddit)**)`;
    }
  } else {
    console.log(`${x} days`);
    return `### Psychedelic Tolerance Calculator\n\n---\n\nYou should not currently have tolerance.\n\n---\n\n^(**[Effect Index](https://www.effectindex.com) • Commands • [Subreddit](https://www.reddit.com/r/DoseBot/) • [Discord](https://discord.gg/KvspajH) • [Github](https://github.com/GabrielMorris/dosebot-reddit)**)`;
  }
}

function generatePercentage(y) {
  return Math.ceil(y / 10) * 10;
}
