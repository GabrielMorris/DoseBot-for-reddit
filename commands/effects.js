const sanitizeSubstanceName = require("../include/sanitize-substance-name.js");

exports.run = (comment, args) => {
  const { request } = require("graphql-request");

  console.log(
    `**********Executing effects on ${
      comment.subreddit_name_prefixed
    }**********`
  );

  let str = comment.body;
  let result = str.split(" ");
  //removes all symbols and puts everything in lower case so bot finds the images easier
  let drug = str
    .toLowerCase()
    .replace("dosebot ", "", -1)
    .replace("effects ", "", -1)
    .replace(/-/g, "", -1)
    .replace(/ /g, "", -1)
    .replace(/\\/, "", -1);
  console.log(drug);
  drug = sanitizeSubstanceName(drug).trim();
  console.log(drug);

  // loads graphql query from separate file as query variable
  const query = require("../queries/effects.js").effect(drug);

  request("https://api.psychonautwiki.org", query)
    .then(data => {
      console.log(data); // SHOW ME WHAT YOU GOT

      if (data.substances.length == 0) {
        comment
          .reply(
            `There are no substances matching \`${drug}\` on PsychonautWiki.`
          )
          .catch(console.error);
        return;
      }

      if (data.substances.length > 1) {
        comment
          .reply(
            `There are multiple substances matching \`${drug}\` on PsychonautWiki.`
          )
          .catch(console.error);
        return;
      }
      const substance = data.substances[0];
      let commentReply = createEffectsList(substance);

      comment.reply(commentReply).catch(console.error);
    })
    .catch(console.log);

  function createEffectsList(substance) {
    const effects = substance.effects;
    const numberOfEffects = effects.length;
    let randomNumberArray = [];
    let namesUnderscoresRemovedArray = [];

    while (randomNumberArray.length < 10) {
      randomNumberArray.push(Math.floor(Math.random() * numberOfEffects));
    }

    randomNumberArray.forEach(element => {
      namesUnderscoresRemovedArray.push(
        effects[element].name.replace(/ /g, "_")
      );
    });

    var messages = [
      `#### ${capitalize(substance.name)} Effect Information\n\n---`
    ];

    // loops through effects and add their name to the message variable
    for (let i = 0; i < randomNumberArray.length; i++) {
      messages.push(
        `- [${
          effects[randomNumberArray[i]].name
        }](https://psychonautwiki.org/wiki/${namesUnderscoresRemovedArray[i]})`
      );
    }
    messages.push(
      `\n\nThese effects were randomly selected from a larger list - [see all effects](https://psychonautwiki.org/wiki/${
        substance.name
      }#Subjective_effects)`
    );

    messages.push(buildBotLinksField());

    return messages.join("\n");
  }

  function buildBotLinksField() {
    return `\n\n---\n\n[Effect Index](https://www.effectindex.com) • [Commands](https://www.reddit.com/r/DoseBot/comments/8xtj1h/commands/) • [Subreddit](https://www.reddit.com/r/DoseBot/) • [Github](https://github.com/GabrielMorris/dosebot-reddit)`;
  }

  function capitalize(name) {
    if (name === "lsd") {
      return name.toUpperCase();
    } else {
      return name[0].toUpperCase() + name.slice(1);
    }
  }
};
