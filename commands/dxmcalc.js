exports.run = (comment, args) => {
  // Calculates DXM plateau dosages
  // usage --dxmcalc [weight in pounds]]
  console.log(
    `**********Executing dxmcalc on ${
      comment.subreddit_name_prefixed
    }**********`
  );

  let str = comment.body;
  let result = str.split(" ");
  let weight = parseFloat(result[result.length - 1]);

  // Check to see if weight is a number and terminate if false
  if (isNaN(weight)) {
    comment.reply("Weight is not a number");
    return;
  }

  // Kaylee's formula for DXM weight calculation
  let calculatedDoseModifier = 2 * getLog(125, weight) - 1;

  comment.reply(generateReply(calculatedDoseModifier));
};

// Function for getting log base 125
function getLog(x, y) {
  return Math.log(y) / Math.log(x);
}

function generateReply(modifier) {
  // Modify dosages by weight
  let lightMin = Math.floor(100 * modifier);
  let lightMaxCommonMin = Math.floor(200 * modifier);
  let commonMaxStrongMin = Math.floor(400 * modifier);
  let strongMaxHeavy = Math.floor(700 * modifier);

  let dosages = {
    lightMin: Math.floor(100 * modifier),
    lightMaxCommonMin: Math.floor(200 * modifier),
    commonMaxStrongMin: Math.floor(400 * modifier),
    strongMaxHeavy: Math.floor(700 * modifier),
    units: "mg"
  };
  let response = `### DXM Dosage Calculator
  ---
  |First plateau|${dosages.lightMin} - ${dosages.lightMaxCommonMin}${
    dosages.units
  }|
  |:-:|:-:|
  |Second plateau|${lightMaxCommonMin} - ${commonMaxStrongMin}${dosages.units}|
  |Third plateau|${commonMaxStrongMin} - ${strongMaxHeavy}${dosages.units}|
  |Fourth plateau|${strongMaxHeavy}${dosages.units}+|

  **Warning:** These recommendations are an approximation, please take into account your own personal tolerance and start with lower dosages. Doses exceeding 1500mg are potentially fatal.

  **More information:** [PsychonautWiki](https://psychonautwiki.org/wiki/DXM) • [Tripsit](http://drugs.tripsit.me/dxm) • [Combination chart](https://wiki.tripsit.me/images/3/3a/Combo_2.png)

  ---

  ^(**[Effect Index](https://www.effectindex.com) • [Commands](https://www.reddit.com/r/DoseBot/comments/8xtj1h/commands/) • [Subreddit](https://www.reddit.com/r/DoseBot/) • [Discord](https://discord.gg/KvspajH) • [Github](https://github.com/GabrielMorris/dosebot-reddit)**)`;

  return response;
}
