require("dotenv").config();

const Snoowrap = require("snoowrap");
const Snoostorm = require("snoostorm");
const CommandSystem = require("./command-system.js")();
const PostReply = require("./commands/post-info.js");

let fs = require("fs");
let path = require("path");
let util = require("util");

// Build Snoowrap and Snoostorm clients
const r = new Snoowrap({
  userAgent: "Dose-Bot",
  clientId: process.env.CLIENT_ID,
  clientSecret: process.env.CLIENT_SECRET,
  username: process.env.REDDIT_USER,
  password: process.env.REDDIT_PASS
});
r.config({
  requestDelay: 1000
});
const client = new Snoostorm(r);

// Subreddits
//// /r/dosebot
// // Configure options for stream: subreddit & results per query
// const doseBotCommentOptions = {
//   subreddit: "DoseBot",
//   results: 25
// };

// // Create a Snoostorm CommentStream with the specified options
// const doseBotComments = client.CommentStream(doseBotCommentOptions);

// // Configure options for submission stream
// const doseBotSubmissionStreamOptions = {
//   subreddit: "DoseBot",
//   results: 5
// }

// // Create a Snoostorm SubmissionStream with the specified options
// const doseBotSubmissions = client.SubmissionStream(doseBotSubmissionStreamOptions);

// // Check post stream for threads with substances in the title
// doseBotSubmissions.on("submission", function(post) {
//   console.log("Online on /r/dosebot (submissions)");
//   PostReply.run(post);
// });

// // On comment execute command
// doseBotComments.on("comment", (comment) => {
//   console.log("Online on /r/dosebot (comments)");
//   CommandSystem.execute(comment);
// });

//// /r/researchchemicals
// Configure options for stream: subreddit & results per query
const researchChemicalsCommentOptions = {
  subreddit: "researchchemicals",
  results: 25
};

// Create a Snoostorm CommentStream with the specified options
// const researchChemicalsComments = client.CommentStream(
//   researchChemicalsCommentOptions
// );

// Create a Snoostorm SubmissionStream with the specified options
const researchChemicalsSubmissionStreamOptions = {
  subreddit: "researchchemicals",
  results: 5
};

// Create a Snoostorm SubmissionStream with the specified options
const researchChemicalsSubmissions = client.SubmissionStream(
  researchChemicalsSubmissionStreamOptions
);

// Check post stream for threads with substances in the title
researchChemicalsSubmissions.on("submission", function(post) {
  console.log("New /r/researchchemicals submission");
  PostReply.run(post);
});

// On comment execute command
// researchChemicalsComments.on("comment", comment => {
//   console.log("New /r/researchchemicals comment");
//   CommandSystem.execute(comment);
// });

// Load command system
CommandSystem.load(function() {
  console.log("Command system loaded");
});
