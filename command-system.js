let fs = require("fs");

module.exports = function CommandSystem() {
  let commandPrefix = "DoseBot ";
  let commandTable = {};

  return {
    load: function(ready) {
      fs.readdir("./commands", function(err, items) {
        for (let i = 0; i < items.length; i++) {
          try {
            let commandName = items[i].replace(/.js$/, "");

            commandTable[commandName] = require(`./commands/${commandName}.js`);
          } catch (error) {
            console.error(
              `Encountered error trying to require command: ${commandName}`
            );
            console.error(error);
          }
        }
        ready();
      });
    },

    execute: function(comment) {
      // If post is by Dose-Bot ignore it
      if (comment.author.name !== "Dose-Bot") {
        // If comment starts with rich text editor's backslash change commandPrefix
        if (comment.body.startsWith("\\")) {
          commandPrefix = "\\DoseBot ";
          console.log("Command detected");
        }
        const args = comment.body
          .slice(commandPrefix.length)
          .trim()
          .split(/ +/g);
        const commandName = args.shift().toLowerCase();
        console.log(`Attempting to execute command: ${commandName}`);
        const commandFunction = commandTable[commandName];

        if (!!commandFunction) {
          try {
            commandFunction.run(comment, args);
          } catch (err) {
            console.error(
              `Encountered error trying to execute command: ${commandName}`
            );
            console.error(err);
          }
        } else {
          console.log(`Command does not exist: ${commandName}`);
        }
      } else {
        console.log("Ignoring post by DoseBot");
      }
    }
  };
};
